class { 'apache': mpm_module => 'prefork', }
apache::vhost { 'vhost.example.com':
  port    => '80',
  docroot => '/var/www/vhost',
}

include apache::mod::php

node default {
    include cron-puppet
    include apache
}