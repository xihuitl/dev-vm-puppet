# Install composer
 
class composer::install {
 
  package { "curl":
    ensure => installed,
  }
 
  exec { 'install composer':
    command => 'curl -sS https://getcomposer.org/installer | sudo php -- --install-dir=/usr/local/bin --filename=composer',
    require => Package['curl'],
  }
 
}
